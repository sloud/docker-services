#!/bin/sh

is_zipped() {
    local db_dump="$1"

    if [ "$db_dump" == *.gz ]; then
        return 0
    else
        return 1
    fi
}

get_db_name() {
    local db_dump="$1"

    echo "$db_dump" | sed 's/\.sql.*//'
}

main() {
    local container_id="$1"
    local db_dump="$2"

    if [ -z "$container_id" ] || [ -z "$db_dump" ]; then
        echo "usage: ./upload-dump.sh \$CONTAINER_ID \$DB_DUMP" && exit 1
    fi

    cp -v "$db_dump" dump.sql

    echo
    echo "Copying dump.sql in container [ $container_id ]..."
    docker cp dump.sql "${container_id}:/dump.sql"

    echo
    echo "Uploading the dump into the server..."
    docker exec ${container_id} sh -c "mysql -u root -ppassword < /dump.sql"
}

main $@
