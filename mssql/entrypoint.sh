#!/bin/sh

init() {
    if [ -f /init.sql ]; then
        for i in {1..50}; do
            /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -d master -i /init.sql
            if [ $? -eq 0 ]; then
                echo "init.sql completed"
                # cat /init.sql
                break
            else
                echo "not ready yet..."
                sleep 1
            fi
        done
    fi
}

init & 
/opt/mssql/bin/permissions_check.sh /opt/mssql/bin/sqlservr


# /opt/mssql-tools/bin/bcp DemoData.dbo.Products in "/usr/src/app/Products.csv" -c -t',' -S localhost -U sa -P "$SA_PASSWORD"