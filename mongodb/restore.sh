main() {
    local connection_string="$1"
    local dump_dir="$2"

    mongorestore --maintainInsertionOrder --numParallelCollections 1 --numInsertionWorkersPerCollection 1 --uri "$connection_string" "$dump_dir"
}

main $@