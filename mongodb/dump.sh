main() {
    local connection_string="$1"
    local dump_dir="${2:-"dumps/$(date +%Y_%m_%dT%H:%M:%S)"}"

    mongodump --uri "$connection_string" --out "$dump_dir"
}

main $@
