#!/bin/sh

docker network create adminer-net
docker network create mysql-net
docker network create elk-net
docker network create rabbitmq-net
docker network create redis-net
docker network create postgres-net
docker network create ingress-net
docker network create mongo-net
docker network create mssql-net