# docker-services

Local docker services used for a local dev environment. Each one comes with a
docker-compose.yml, so starting a service is as simple as running `docker-compose up -d`
from inside the service directory.

## docker

Everything is built around on `docker` and `docker-compose` so you will need both of those
installed. All the relevant docs can be found [here](https://docs.docker.com/).

## services

Before you can start any of the services you need to set up their networking layer. Simply
run `networks/init.sh`. None of the services run by default, so you will need to start any
of the ones you want by running `docker-compose up -d` inside the service directory.

### **mysql**

- external networks:
  - `mysql-net`
- services:
  - `mysql`
- ports:
  - `3306`
- users
  - `root:password`

### **postgres**

- external networks:
  - `postgres-net`
- services:
  - `postgres`
- ports:
  - `5432`
- users
  - `root:password`

### **adminer**

Runs a local [adminer](https://www.adminer.org/) for administrating databases. It
currently sits on both `mysql-net` and `postgres-net`, so if they are running you can
access them in `adminer` by their service names (_mysql_ or _postgres_ in the **server**
field)

- ports:
  - `18080`

### **rabbitmq**

Runs a local rabbitmq instance with a couple pluggins enabled.

- external networks:
  - `rabbitmq-net`
- services:
  - `rabbitmq`
- ports:
  - `5672`
  - `15672` _(admin ui)_
- users
  - `admin:admin`

### **redis**

- external networks:
  - `redis-net`
- services:
  - `redis`
- ports:
  - `6379`

### **elk**

Runs a local elk stack including both elasticsearch and kibana.

- external networks:
  - `elk-net`
- services:
  - `elasticsearch`
  - `kibana`
- ports:
  - `9200` _(elasticsearch)_
  - `5601` _(kibana)_
